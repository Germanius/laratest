## Install app

To install this app go to app root directory and run this commands
```
composer install
npm install
npm run dev
```
Then setup your database connection in .env file and run migrations with seeder
```
php artisan db:migrate --seed
```
then you can test app with phpunit
```
vendor/bin/phpunit
```