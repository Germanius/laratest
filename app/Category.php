<?php

namespace App;

use Baum\Node;

/**
 * Category
 */
class Category extends Node
{

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'categories';

    public function items()
    {
        return $this->hasMany(Item::class);
    }

}
