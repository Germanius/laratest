<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreItem;
use App\Item;
use Faker\Generator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categoryId = $request->input('category_id');
        if (empty($categoryId)){
            $items = Item::all();
        } else {
            $items = Item::where('category_id', $categoryId)->get();
        }

        return response($items->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Generator $faker
     * @return \Illuminate\Http\Response
     */
    public function store(StoreItem $request)
    {
        $item = Item::create([
            'category_id' => $request->category_id,
            'name' => $request->name,
            'description' => $request->description,
            'image' => $request->image
        ]);
        $item->save();

        return response($item->jsonSerialize(), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(Item::findOrFail($id)->jsonSerialize(), Response::HTTP_OK);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function update($id, StoreItem $request)
    {
        $item = Item::findOrFail($id);
        $item->category_id = $request->category_id;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->image = $request->image;
        $item->save();

        return response($item->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        $item->delete();

        return response('Item deleted succefully', Response::HTTP_OK);
    }
}
