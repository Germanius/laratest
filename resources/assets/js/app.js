/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';

Vue.use(VueRouter);

import Items from './components/ItemsComponent.vue';
import CategoryNavigation from './components/CategoryNavigationComponent.vue';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Items
        },
        {
            path: '/?category_id=:categoryId',
            name: 'category-items',
            component: Items
        }
    ]
});

const app = new Vue({
    el: '#app',
    components: {
        'items': Items,
        'category-navigation': CategoryNavigation
    },
    router
});
