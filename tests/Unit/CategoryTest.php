<?php
/**
 * Created by PhpStorm.
 * User: german
 * Date: 16.06.2018
 * Time: 9:19
 */

namespace Tests\Unit;

use App\Category;
use Illuminate\Http\Request;
use PhpParser\Node\Scalar\String_;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class CategoryTest
 * @package Tests\Unit
 */
class CategoryTest extends TestCase
{
    /**
     * Test show list of all categories.
     * @test
     * @return void
     * @category
     *
     */
    public function itCanSeeListOfCategories()
    {
        $category = Category::all()->first();

        $this->getJson('/api/category/')
            ->assertStatus(200)
            ->assertJsonStructure([$category->id]);
    }

    /**
     * Test show single category.
     * @test
     * @return void
     *
     */
    public function itCanSeeSingleCategory()
    {
        $category = Category::all()->first();

        $this->getJson("/api/category/{$category->id}")
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'parent_id',
                'lft',
                'rgt',
                'depth',
                'name',
                'created_at',
                'updated_at'
            ]);
    }

    /**
     * @test
     * @return void
     *
     */
    public function itCanCreateCategory()
    {
        $parentCategory = Category::all()->last();
        $parentId = ($parentCategory) ? $parentCategory->id : null;
        $postBody = [
            'name' => "Electronics",
            'parent_id' => $parentId
        ];

        $this->postJson('/api/category/', $postBody)
            ->assertStatus(201)
            ->assertJsonStructure([
                'id',
                'parent_id',
                'lft',
                'rgt',
                'depth',
                'name',
                'created_at',
                'updated_at'
            ])
            ->assertJsonFragment($postBody);

    }

    /**
     * @test
     * @return void
     */
    public function itCanUpdateCategory()
    {
        $category = Category::all()->last();
        $postBody = [
            'name' => 'TV & Audio',
            'parent_id' => null
        ];

        $this->putJson("/api/category/{$category->id}", $postBody)
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'parent_id',
                'lft',
                'rgt',
                'depth',
                'name',
                'created_at',
                'updated_at'
            ])
            ->assertJsonFragment($postBody);
    }


    /**
     * @test
     * @return void
     */
    public function itCanDeleteCategory()
    {
        $category = Category::all()->last();

        $this->deleteJson("/api/category/{$category->id}")
            ->assertStatus(200);
    }


}