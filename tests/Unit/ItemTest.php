<?php
/**
 * Created by PhpStorm.
 * User: german
 * Date: 26.05.2018
 * Time: 18:19
 */

namespace Tests\Unit;

use App\Category;
use App\Item;
use PhpParser\Node\Scalar\String_;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

Class ItemTest extends TestCase
{
    /**
     * Test show list of all items.
     * @test
     * @return void
     *
     */
    public function itCanSeeListOfItems()
    {
        $this->getJson('/api/items/')
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id', 'category_id', 'name', 'description', 'image', 'created_at', 'updated_at'
                ]
            ]);
    }

    /**
     * Test show single item of all items.
     * @test
     * @return void
     *
     */
    public function itCanSeeSingleItem()
    {
        $item = Item::all()->first();

        $this->getJson("/api/item/{$item->id}")
            ->assertStatus(200)
            ->assertJsonStructure([
                'id', 'category_id', 'name', 'description', 'image', 'created_at', 'updated_at'
            ]);
    }

    /**
     * @test
     * @return void
     *
     */
    public function itCanCreateItem()
    {
        $category = Category::all()->first();
        $postBody = [
            'category_id' => $category->id,
            'name' => "Test item",
            'description' => 'Test description',
            'image' => 'test_image.jpg'
        ];

        $this->postJson('/api/item/', $postBody)
            ->assertStatus(201)
            ->assertJsonStructure(['id', 'category_id', 'name', 'description', 'image', 'created_at', 'updated_at'])
            ->assertJsonFragment($postBody);
    }

    /**
     * Test to update item.
     * @test
     * @return void
     *
     */
    public function itCanUpdateItem()
    {
        $category = Category::all()->first();
        $item = Item::all()->last();
        $postBody = [
            'category_id' => $category->id,
            'name' => "Test item",
            'description' => 'Test description',
            'image' => 'test_image.jpg'
        ];

        $this->putJson("/api/item/{$item->id}", $postBody)
            ->assertStatus(200)
            ->assertJsonStructure(['id', 'category_id', 'name', 'description', 'image', 'created_at', 'updated_at'])
            ->assertJsonFragment($postBody);
    }

    /**
     * @test
     * @return void
     */
    public function itCanDeleteItem()
    {
        $item = Item::all()->last();

        $this->deleteJson("/api/item/{$item->id}")
            ->assertStatus(200);
    }
}