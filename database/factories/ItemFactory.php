<?php
/**
 * Created by PhpStorm.
 * User: german
 * Date: 26.05.2018
 * Time: 15:49
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

$factory->define('App\Item', function ($faker) {
    return [
        'category_id' => null,
        'name' => $faker->word,
        'description' => $faker->paragraph,
        'image' => $faker->imageUrl($width = 300, $height = 200, 'technics'),
    ];
});


