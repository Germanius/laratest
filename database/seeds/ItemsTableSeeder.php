<?php

use App\Category;
use App\Item;
use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();

        foreach ($categories as $category) {
            factory(Item::class, 8)->create([
                'category_id' => $category->id
            ]);

        }
    }
}
