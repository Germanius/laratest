<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    protected $seeders = [
        'CategoriesTableSeeder' => 'categories',
        'ItemsTableSeeder' => 'items'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->seeders as $seeder => $table) {
            DB::table($table)->truncate();
            $this->call($seeder);
        }
    }
}
